package example

import (
	"encoding/json"
)

type Aluno struct {
	Nome  string `json:"nome"`
	Email string `json:"email"`
}

func (a *Aluno) ToJson() ([]byte, error) {
	return json.Marshal(a)
}
