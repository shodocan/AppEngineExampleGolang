package example

import (
	"context"

	"cloud.google.com/go/datastore"
)

type AlunoController struct {
	db  *datastore.Client
	ctx context.Context
}

func NewAlunoController(ctx context.Context) (*AlunoController, error) {
	controller := new(AlunoController)
	client, err := datastore.NewClient(ctx, "infoweekexample")
	controller.db = client
	controller.ctx = ctx
	return controller, err
}

func (controller *AlunoController) PersisteAluno(aluno *Aluno) (int64, error) {
	key := datastore.IncompleteKey("Aluno", nil) // incomplete key é pra gerar o id automatico, para especificar um id use NameKey
	key, err := controller.db.Put(controller.ctx, key, aluno)
	return key.ID, err
}
