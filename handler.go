package example

import (
	"encoding/json"
	"net/http"

	"google.golang.org/appengine"
)

type AlunoHandler struct {
}

func (*AlunoHandler) Handle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Add("Access-Control-Allow-Origin", "*")
	ctx := appengine.NewContext(r)
	nome := r.URL.Query().Get("nome")
	email := r.URL.Query().Get("email")
	aluno := &Aluno{Nome: nome, Email: email}
	if aluno.Nome == "" || aluno.Email == "" {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(aluno)
		return
	}

	controller, err := NewAlunoController(ctx)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	id, err := controller.PersisteAluno(aluno)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err)
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(id)
}
